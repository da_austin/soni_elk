# Sonitrol (sonitrol/jake/)
### Overview
The sonitrol application can be easily deployed to a cloud environment. It consists of 3 components:

- A listener (sonitrol-listener)
- A cache (Elasticsearch)
- A visualization dashboard (Kibana)

Architecture				 
:---------------------------:
![](/img/Overview.png)  

The sonitrol-listener consists of 4 layers, shown below. The visuals can be updated in near-real time, and are derived from the SonIP directory. All metadata can be easily filtered and queried using Elasticsearch.

Sonitrol-Listener
:--------------------------:
![](/img/AI.png)

### Cloning
To clone this repository, execute the following command from your (local) terminal:
```
$ git clone https://<username>@bitbucket.org/sbdDigitalAccelerator/sonitrol.git
```
where **<username>** is a placeholder for your username. You should then be able to access the contents of this directory locally. Please consider installing docker as well via the links referenced above.

### Versions
This application supports hostile/not-hostile prediction (via VGGish/XGBoost AI model) and near-real time visualization (via Elasticsearch/Kibana). All code is property of Stanley Black & Decker (2018). Our application currently supports the following software versions:

- Python 2.7
- Tensorflow 1.6.0
- scikit-learn 0.19.1
- Elasticsearch 6.2
- Kibana 6.2.2
- Docker 18.03.0-ce
- Compose (Docker) 1.20.1

Probably the easiest way to develop against all dependencies is by using docker. A description of docker, along with supporting documentation for local installation, can be found here: [Install Docker | Docker Documenation](https://docs.docker.com/install/). A full overview of the requirements is contained in requirements.txt (a reduced docker version of the dependencies is contained in docker-reduced-requirements.txt, only applicable if installing within tensorflow/tensorflow docker container).

**Note:** It is *very* important that the versions of Kibana and Elasticsearch match - so feel free to upgrade these versions, but you *must* upgrade them together. View the [Elastics Support Matrix](https://www.elastic.co/support/matrix#matrix_compatibility) for more details. If launching Elasticsearch and Kibana from AWS, these versions will automatically match.

### Elasticsearch/Kibana Instance (Separate AWS Instance(s))
You can spawn and Amazon Elasticsearch Service cluster using the following instructions: [Amazon Elasticsearch Service](https://aws.amazon.com/elasticsearch-service/). This service provides its own Kibana endpoint, as well as a dedicated elasticsearch REST endpoint. You need to spawn this instance via the AWS console and record these endpoints for integration with the sonitrol-listener later (described below).

## AWS Automated Deployment
### Sonitrol-Listener (Dedicated EC2 Node)
First, spawn and Elasticsearch Service instance via the AWS console (outlined in the section above). Next, spawn another EC2 instance with at least 8GB of RAM. This secondary EC2 node can be configured by running the aws-config-ubuntu.sh shell script (located in the sonitrol/jake/ repo - this assumes you launched a *ubuntu* EC2 instance). Verify that the shell script is set as executable (chmod +x aws-config-ubuntu.sh). The script should install the docker and docker-compose dependencies, build the docker images, and deploy them to an EC2 node. (*Note:* Currently running/tested on a m5.xlarge node).

Before running this script, you must update/verify the following dependencies:

- ssh into your EC2 instance
```
$ ssh -i <path-to-key.pem> ubuntu@<ec2-public-ip>
```

- Clone the sonitrol repository from bitbucket to your EC2 instance in the home (~/) directory
```
$ git clone https://<username>@bitbucket.org/sbdDigitalAccelerator/sonitrol.git
```

- Mount the SonIP Directory (*must do this step through IT - talk with Coop if this isn't configured on your EC2 instance*) and update the jake/docker/docker-compose-listener.yml file to listen to this volume
**docker-compose-listener.yml**
```
17        - <MOUNTED_AWS_DIRECTORY>:/workspaces/sonitrol/data/
```

- Point Services Towards Your AWS Elasticsearch Instance in the jake/docker/docker-compose-listener.yml file
**docker-compose-listener.yml**
```
19          - "ELASTICSEARCH_HOST=vpc-coop-sss-yg4vszbibo6wiftocnrxr2p5ju.us-east-1.es.amazonaws.com"
20          - "ELASTICSEARCH_PORT=80"
```

Note, on AWS elasticsearch operates out of port 80. You do not need to add the http or https protocol. In the ~/sonitrol/jake/ folder, run the configuration script.

```
$ ./aws-config-ubuntu.sh
```

You should see several docker logs print out immediately, followed by the service starting up and listening to the directory. You'll notice a log that says something like: "MONITOR - Listening to Files at /workspaces/sonitrol/data/04-14-2018/". The date portion should be correct (it should match today's date - or the most recent date folder in SonIP), but the /workspaces/sonitrol/data will be different than your mounted directory on AWS. This is because /workspaces/sonitrol/data is the local path with the docker container. As long as your AWS path was referenced correctly in the docker-compose-listener.yml, you should be fine.

A few notes about the listener:

1. **Data Consistency**: The listener is designed to *begin* listening for new files on startup. That means that whatever files are already in the mounted folder will not be read in. The listener will automatically mount to the most recent date folder within SonIP. When a new date folder is generated, it will detect that and begin listening to it. This means that any files added to older directories by SonIP after that point will not be read in once a new directory is created.

2. **Failure**: Should the EC2 instance or listener fail, it can be rebooted using the steps above. However, as noted in 1., it will only begin listening to new files created after startup. Therefore, files generated by SonIP while it was down will not be detected or analyzed (or duplicated in Elasticsearch).

3. **Memory Leaks**: I have some concerns about the space complexity of the monitor function. It tracks ingested audio file names in RAM - but resets itself to [] once a new directory is created (daily). While these are just string references of around 30,000 - 50,000 names, I have not verified that this gets reset when a new directory is created (although the code is built to do so). This *could* lead to problems in production.

Ultimately, the listener should monitor the files in ES, the files in SonIP SQL, and check for consistency/differences both on startup and during operation. It doesn't currently do this, pursuant to the caveats above. Regardless, for a long-term production application it should be designed this way.   

## Configuring Sonitrol-Listener (Docker)
#### Deployment
Logic for the sonitrol-listener/ES client is contained in the sonitrol-listener.py file. The listener monitors mp3/.wav files that are added to a user-defined directory. When a new file is added, the listener loads it into memory and runs it through a pre-trained VGG/XGBoost model - generating a prediction on hostile/not-hostile. There is metadata that is also determined based on the audio-file name. This metadata is stored in Elasticsearch via an ES python client. A user can define the directory to monitor at startup by modify the following line in the docker/docker-compose.yml file:
```
13     sonitrol-api:
14       image: sonitrol/listener:1.0
15       container_name: sonitrol-listener
16       volumes:
17        - <MOUNTED_AWS_DIRECTORY>:/workspaces/sonitrol/data/
```
This line specifies where to mount the volume to the data directory for the sonitrol-listener container. Modify it according to the parameters of your environment. Alternatively, you can modify this input at startup via the following command:
```
$ docker run -it -v <MOUNTED_AWS_DIRECTORY>:/workspaces/sonitrol/data/ --name sonitrol-listener sonitrol/listener:1.0
```

#### Building the sonitrol/listener image
The sonitrol-listener can be built directly from this repository. Execute the following command in the jake/ directory:
```
$ docker build -t sonitrol/listener:1.0 -f docker/Dockerfile.listener .
```
Note, if you need to add new libraries to a file, please update the docker/Dockerfile accordingly. The Dockerfile ultimately serves as a proxy for bash commands, so it should be obvious how to edit it.

When the build complete (it may take a few minutes), you should see the image stored in your local docker cache. Execute the following command to see it:
```
$ docker images
```

**Note:** The 1.0 version tag can be modify as-needed. This is a standard version tag, and should increment as you implement changes.

#### Modifying ML Parameters
The sonitrol-listener makes predictions according to various user-defined parameters (including prediction threshold, pre-trained model, etc). These parameters are stored in the jake/utils/production-params.py file. This file lists all runtime parameters used by the sonitrol-listener.


#### ES Local
Follow the instructions here for installing Elasticsearch locally: [Installing and Running Elasticsearch](https://www.elastic.co/guide/en/elasticsearch/guide/current/running-elasticsearch.html).

You can also install docker first (see above) and then run elasticsearch using the following commands:
```
$ docker pull elasticsearch:<version>
$ docker run -it -p 9200:9200 -p 9300:9300 --name elasticsearch elasticsearch:<version>
```

#### ES Cloud
Finally, you can launch and AWS ES instance by following these instructions:[What Is Amazon Elasticsearch Service](https://docs.aws.amazon.com/elasticsearch-service/latest/developerguide/what-is-amazon-elasticsearch-service.html).

**Note:** Using the AWS ES option could incur additional costs, please check on pricing before developing against this option (mostly intended for production)

## Configuring Kibana
Kibana is a visualization tool built specifically for Elasticsearch. It enables near-real time monitoring of data, and can readily scale in production cloud environments. It also supports custom visualizations via plugins. By default, Kibana is served at port 5601.

#### Kibana Local
Kibana can be run locally using the following options: [Kibana Local Installation](https://www.elastic.co/guide/en/kibana/current/setup.html),
or run via docker:
```
$ docker pull kibana:<version>
$ docker run -it -p 5601:5601 --name kibana kibana:<version>
```

#### Kibana Cloud
Kibana is automatically installed when launching ES vis AWS. The AWS console should provide a Kibana endpoint on startup. From a load-balancing perspective, this is the preferred way to operate in production anyway. Some customizations may be difficult to implement in this case - additional research should be conducted to verify this approach.

## Data Replication/Consistency
Since this tool provides visualization capability using a secondary cache (Elasticsearch), data replication and consistency is very important. This prototype implements an "asynchronous replication / eventual consistency" scheme. It will monitor SonIP for new updates, and write these updates as they come in. However, there will be a delay between when these files are written and when they are viewable in ES/Kibana. Going forward, we will likely implement additional safeguards to account for fail over/ make the service more fault tolerant (however, this could add additional latency).
