"""
AUTHOR: Jake Tutmaher
MAINTAINER: Stanley Black & Decker
LAST UPDATED: 3-23-18

Methods for utilizing pre-trained AudioSet VGGish Model
See TF documentation for more detail:
"""

import numpy as np
from numpy.random import shuffle
import pandas as pd
import sys
from tqdm import tqdm
from utils import params
from utils import dsp as signal
from utils import augment

from scipy.io import wavfile

import tensorflow as tf

import models.research.audioset.vggish_params as vggish_params
from models.research.audioset.vggish_input  import wavfile_to_examples, waveform_to_examples
from models.research.audioset.mel_features import log_mel_spectrogram
import models.research.audioset.vggish_input as vggish_input
import models.research.audioset.vggish_slim as vggish_slim

def _pre_emphasis_filter(wfile,pre_emphasis=0.97):
    rate,wave = wavfile.read(wfile)
    assert wave.dtype == np.int16, 'Bad sample type: %r' % wav_data.dtype
    swave = wave / 32768.0
    dwave = np.append(swave[0], swave[1:] - pre_emphasis * swave[:-1])
    return waveform_to_examples(dwave,rate)


def feature_extract(wfiles,wcsv,checkpoint=params.VGG_CHECKPOINT_LOAD,window_length=params.WINDOW_LENGTH,
    hop_length=params.HOP_LENGTH,window_seconds=params.WINDOW_SECONDS,
    hop_seconds=params.HOP_SECONDS,sample_rate=params.SAMPLE_RATE,
    dsp=False,lowcut=200,highcut=8000,aug=False,pre_emphasis=0.97):
    """
    Method for generating 5x128 embedding vectors for 5s audio clips from
    pretrained VGGish TF model
    
    :param: files - list of audio .wav files to be analyzed
    :param: frame - pd dataframe containing labels
    :return: data & labels - np.array of data (n_records x 640) and list of labels
    """
    #Melgram Parameters
    tf.reset_default_graph()
    vggish_params.STFT_WINDOW_LENGTH_SECONDS = window_length
    vggish_params.STFT_HOP_LENGTH_SECONDS = hop_length
    vggish_params.EXAMPLE_WINDOW_SECONDS = window_seconds
    vggish_params.EXAMPLE_HOP_SECONDS = hop_seconds
    vggish_params.SAMPLE_RATE = sample_rate

    # In this simple example, we run the examples from a single audio file through
    # the model. If none is provided, we generate a synthetic input.
    dframe = pd.concat((pd.read_excel(f) for f in wcsv))
    embedding = {}
    labels = {}
    with tf.Session() as sess:
        # Define the model in inference mode, load the checkpoint, and
        # locate input and output tensors.
        vggish_slim.define_vggish_slim(training=False)
        vggish_slim.load_vggish_slim_checkpoint(sess, checkpoint)
        features_tensor = sess.graph.get_tensor_by_name(vggish_params.INPUT_TENSOR_NAME)
        embedding_tensor = sess.graph.get_tensor_by_name(vggish_params.OUTPUT_TENSOR_NAME)
        
        for wfile in wfiles:
            print "-- Step: "+str(len(embedding)+1)
            print "Inferencing File: "+wfile
            name = wfile.split("/")[-1]
            try:
                label = int(np.unique(dframe[dframe['file_name']==name]['num_label']))
            except:
                print "SKIPPING - FOUND MULTIPLE LABELS FOR THIS ENTRY"
                continue
            mel_temp = wavfile_to_examples(wfile)
            if mel_temp.shape!=(5,96,64):
                print "Error - Incorrect File Shape, Skipping"+str(mel_temp.shape)
                continue
            else:
                if aug:
                    # Standard
                    [embedding_batch] = sess.run([embedding_tensor],feed_dict={features_tensor: mel_temp})
                    embedding[wfile] = np.array(embedding_batch).flatten() 
                    labels[wfile] = label

                    # Shifted
                    [embedding_batch_1] = sess.run([embedding_tensor],feed_dict={features_tensor: np.roll(mel_temp,3,axis=0)})
                    embedding[wfile+"_roll"] = np.array(embedding_batch_1).flatten() 
                    labels[wfile+"_roll"] = label

                    # White Noise
                    [embedding_batch_2] = sess.run([embedding_tensor],feed_dict={features_tensor: augment.white_noise(wfile)})
                    embedding[wfile+'_noise'] = np.array(embedding_batch_2).flatten()
                    labels[wfile+'_noise'] = label

                    # White Noise + Shifted
                    [embedding_batch_3] = sess.run([embedding_tensor],feed_dict={features_tensor: np.roll(augment.white_noise(wfile),3,axis=0)})
                    embedding[wfile+'_noise_roll'] = np.array(embedding_batch_3).flatten()
                    labels[wfile+'_noise_roll'] = label
                else:
                    # Standard
                    [embedding_batch] = sess.run([embedding_tensor],feed_dict={features_tensor: mel_temp})
                    embedding[wfile] = np.array(embedding_batch).flatten()
                    labels[wfile] = label 

    return embedding,labels

def retrain(xdata,ydata,nclasses=2,checkpoint=params.VGG_CHECKPOINT_LOAD,fout=params.VGG_CHECKPOINT_SAVE,nepochs=20,batch_size=32,test_size=0.01):
    """
    Method for retraining Youtube-8M VGGish starting from pre-trained weights.
    !!! TO-DO integrate in validation step after each epoch for training. 

    :param: xdata - numpy array (mel_spectrograms) for fitting model to
    :param: ydata - list (nrecords) containing discrete classes for classification
    :param: nclasses - int number of classes to classify data into
    :param: checkpoint - str path to locations of VGGish Youtube-8M weights (up to embedding layer)
    :param: fout - str output to stored retrained weights at
    :param: nepochs - number of epochs to retrain model for
    :param: batch_size - int number of records to include in each batch
    :param: test_size - float ratio of samples to hold out for validation purposes
    """
    
    with tf.Graph().as_default(),tf.Session() as sess:
        # Define Embeddings
        embeddings = vggish_slim.define_vggish_slim(training=True)
        # Access the graph - get variable names
        vggish_var_names = [v.name for v in tf.global_variables()]
        vggish_vars = [v for v in tf.global_variables() if v.name in vggish_var_names]

        with tf.variable_scope('mymodel'):
            num_units=100
            fc = slim.fully_connected(embeddings,num_units)

            logits = slim.fully_connected(fc,nclasses,activation_fn=None,scope='logits')
            predictions = tf.sigmoid(logits,name='prediction')

            global_step = tf.Variable(0,name='global_step',trainable=False,collections=[tf.GraphKeys.GLOBAL_VARIABLES,tf.GraphKeys.GLOBAL_STEP])
            labels = tf.placeholder(tf.float32,shape=(None,nclasses),name='labels')

            xent = tf.nn.sigmoid_cross_entropy_with_logits(logits=logits,labels=labels,name='xent')
            loss = tf.reduce_mean(xent,name='loss_op')
            acc,acc_op = tf.metrics.accuracy(labels=tf.argmax(labels,1),predictions=tf.argmax(predictions,1),name='acc_op')
            tf.summary.scalar('loss',loss)
            tf.summary.scalar('acc',acc)

            optimizer = tf.train.AdamOptimizer(learning_rate=1e-4,epsilon=1e-8)
            optimizer.minimize(loss,global_step=global_step,name='train_op')

        # Initialize Variables of Interest
        sess.run(tf.global_variables_initializer())
        sess.run(tf.local_variables_initializer())

        # Load Pretrained Weights Up To Embeddings Check
        saver = tf.train.Saver(vggish_vars,name='vggish_load_pretrained',write_version=1)
        saver.restore(sess,checkpoint)

        # Call Relevent Tensors
        features_tensor = sess.graph.get_tensor_by_name(vggish_params.INPUT_TENSOR_NAME)
        labels_tensor = sess.graph.get_tensor_by_name('mymodel/labels:0')
        global_step_tensor = sess.graph.get_tensor_by_name('mymodel/global_step:0')
        loss_tensor = sess.graph.get_tensor_by_name('mymodel/loss_op:0')
        acc_tensor = sess.graph.get_tensor_by_name('mymodel/acc_op/value:0')
        pred_tensor = sess.graph.get_tensor_by_name('mymodel/prediction:0')
        train_op = sess.graph.get_operation_by_name('mymodel/train_op')

        #Progress Bar
        counter = int(xdata.shape[0]/float(batch_size))

        # The training loop.
        for _ in range(nepochs):
            print("-- Epoch: "+str(_+1))
            pbar = tqdm(range(counter))
            for step in pbar:
                start = step*batch_size
                stop = start+batch_size
                batch_dict = {features_tensor:xdata[start:stop],labels_tensor:ydata[start:stop]}
                #batch_dict = tf.train.batch({features_tensor:xdata,labels_tensor:ydata},batch_size=batch_size)
                [num_steps, train_acc, train_loss, _] = sess.run([global_step_tensor, acc_op,loss_tensor, train_op],feed_dict=batch_dict)
                pbar.set_postfix(LogLoss=train_loss,Accuracy=train_acc,refresh=False)
            
        save_path = saver.save(sess,fout)
        print "Model saved at path: "+fout
        return

def evaluate(X_test,y_test,checkpoint=params.VGG_CHECKPOINT_SAVE,nclasses=2):
    """
    Method for evaluating test data test data from retrained VGGish, includes
    classification layer. NOTE - the nclasses MUST match those specified in 
    the retraining method. The retraining method should be run prior to the 
    evaluate method.

    :param: X_test - numpy array (mel_spectrograms) to evaluate
    :param: y_test - list (nrecords) of labels for test data
    :param: checkpoint - str file path to retrained checkpoint (see fout in
            retrain)
    :param: nclasses - int number of classes in the classification layer
    """

    with tf.Graph().as_default(),tf.Session() as sess:
        # Define VGGish Up To Embeddings
        embeddings = vggish_slim.define_vggish_slim(training=True)

        # Define FC Classifier After Embeddings
        with tf.variable_scope('mymodel'):
            num_units=100
            fc = slim.fully_connected(embeddings,num_units)

            logits = slim.fully_connected(fc,nclasses,activation_fn=None,scope='logits')
            predictions = tf.sigmoid(logits,name='prediction')

            labels = tf.placeholder(tf.float32,shape=(None,nclasses),name='labels')

            xent = tf.nn.sigmoid_cross_entropy_with_logits(logits=logits,labels=labels,name='xent')
            loss = tf.reduce_mean(xent,name='loss_op')
            acc,acc_op = tf.metrics.accuracy(labels=tf.argmax(labels,1),predictions=tf.argmax(predictions,1),name='acc_op')
        
        #Initialize Tensors
        sess.run(tf.global_variables_initializer())
        sess.run(tf.local_variables_initializer())

        # Load Saved Model (Must be exact replica of what's defined above)
        saver = tf.train.Saver(write_version=1)
        saver.restore(sess,checkpoint)

        # Call Relevent Tensors
        features_tensor = sess.graph.get_tensor_by_name(vggish_params.INPUT_TENSOR_NAME)
        labels_tensor = sess.graph.get_tensor_by_name('mymodel/labels:0')
        loss_tensor = sess.graph.get_tensor_by_name('mymodel/loss_op:0')
        acc_tensor = sess.graph.get_tensor_by_name('mymodel/acc_op/value:0')

        # Evaluate
        [test_loss,test_acc] = sess.run([loss_tensor,acc_op],feed_dict={features_tensor:X_test,labels_tensor:y_test})

        return test_loss,test_acc



