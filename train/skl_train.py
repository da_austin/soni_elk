"""
AUTHOR: Jake Tutmaher
MAINTAINER: Stanley Black & Decker
LAST UPDATED: 3-23-18

Methods for training sklearn classifiers
"""

import warnings
import numpy as np

from sklearn.model_selection import cross_val_score
from sklearn.model_selection import StratifiedKFold
from sklearn.metrics import accuracy_score,log_loss,auc,recall_score,roc_curve 


warnings.filterwarnings("ignore",category=DeprecationWarning)

def train_and_eval(xs,ys,clf,xgb=False,folds=10):
    """
    Method for training a classifier over a defined number of folds
    (default ten) using the Stratified K-Fold method. Stratefied k-
    fold ensures that the class distribution is preserved within each
    fold
    
    :param: xs - np array (nrecords x nfeatures) to be split into k-fold 
            train/val sets
    :param: ys - corresponding list of labels in correct order
    :param: clf - sklearn classifier to fit data to, initialized with 
            specific params
    :param: xgb - boolean value specifying if using XGBoost classifier
            or not. XGBoost must be run in a slightly different manner
    :param: folds - int value specifying number of stratified k-folds
    :return: dict of accuracy metrics from the training/validation
    """
    
    # Define Metric Lists:
    clf_acc = []
    clf_log = []
    clf_auc = []
    clf_rec = []
    
    # Set Up K-Folds
    skf = StratifiedKFold(n_splits=folds, shuffle=True)
    
    # Train and Eval Folds
    for train_idx,test_idx in skf.split(xs,ys):
        # Train/Test Splits
        x_train,y_train = xs[train_idx],np.array(ys)[train_idx]
        x_test,y_test = xs[test_idx],np.array(ys)[test_idx]
    
        # Fit Classifier
        clf_temp = clf
        if xgb:
            clf_temp.fit(x_train,
                         y_train,
                         eval_set=[(x_test, y_test)],
                         verbose=False,
                         eval_metric='logloss',
                         early_stopping_rounds=50)
        else:
            clf_temp.fit(x_train,y_train)
        
        # Gen Eval Stats
        acc = accuracy_score(y_test,clf_temp.predict(x_test))
        ll = log_loss(y_test,clf_temp.predict_proba(x_test))
        rec = recall_score(y_test,clf_temp.predict(x_test))
        fpr, tpr, thresholds = roc_curve(y_test, clf_temp.predict(x_test), pos_label=1)
        mauc = auc(fpr,tpr)
        
        #Append to Lists
        clf_acc.append(acc)
        clf_log.append(ll)
        clf_rec.append(rec)
        clf_auc.append(mauc)
        
        # Print Info to Console
        print "Fold: "+str(len(clf_auc))
        print "Acc: "+str(acc)+" | Logloss: "+str(ll)+" | Recall: "+str(rec)+" | AUC: "+str(mauc)
       
    return {'acc':clf_acc,'log':clf_log,'auc':clf_auc,'rec':clf_rec} 