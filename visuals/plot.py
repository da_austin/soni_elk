
"""
AUTHOR: Jake Tutmaher
MAINTAINER: Stanley Black & Decker
LAST UPDATED: 3-23-18

Methods for plotting data/results of Sonitrol runs
"""

import numpy as np
import matplotlib.pyplot as plt
import itertools
from utils import params


def plot_training_results(clf_metrics,clf_names,nfolds=10,style='acc'):
    """
    Method for plotting training results for a list of classifiers
    :param: clf_metrics - dictionary of metrics for the trained classifier
    :param: clf_names - list of classifier names (this should be switched)
            soon
    :param: nfolds - number of folds that the training was performed over
    :param: style - style of plot, such as acc, rec, log, auc
    """

    xrng = np.linspace(1,nfolds,nfolds)
    colors = ['darkred','darkblue','darkgreen']
    scolors = ['red','blue','green']
    cidx = 0 
    
    plt.figure(figsize=(15,7))
    plt.title("Training Metrics - "+style,size=24)
    
    for clf in clf_metrics:
        plt.plot(xrng,clf[style],color=colors[cidx%len(colors)])
        plt.hlines(np.mean(clf[style]),1,len(xrng),
                   color=scolors[cidx],linestyle="--",
                   label=clf_names[cidx]+" - "+"{:3.3f}".format(np.mean(clf[style])))
        cidx+=1

    plt.grid(color="gray",linestyle="--")
    plt.ylabel(style,size=20)
    plt.xlabel("K-Fold Iteration",size=20)
    plt.yticks(size=20)
    plt.xticks(size=20)
    plt.axis([1,nfolds,0,1])

    plt.legend(prop={'size': 20})
    plt.show()
    
    return

def plot_labels(labels,key=params.orig_key):
    """
    Method to plot the class distribution of the labels based on
    a user-defined key. Depends on numpy to return the unique 
    counts, etc.
    
    :param: labels - list of integer labels
    :param: key - dict/map of int to str values
    """
    bins,counts = np.unique(labels,return_counts=True)
    xs = range(len(bins))
    names = [key[x] for x in bins]
    
    plt.figure(figsize=(15,7))
    plt.title("Class Distribution",size=24)
    plt.ylabel("Counts",size=20)
    plt.xlabel("Class",size=20)
    
    plt.bar(xs,counts)
    plt.grid(color='gray',linestyle='--')
    plt.yticks(size=20)
    plt.xticks(xs,names,size=20,rotation=60)
    
    plt.show()
    
    return

def plot_confusion_matrix(cm, classes,
                          normalize=False,
                          title='Confusion matrix',
                          cmap=plt.cm.Blues):
    """
    This function prints and plots the confusion matrix.
    Normalization can be applied by setting `normalize=True`.
    """
    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
        print("Normalized confusion matrix")
    else:
        print('Confusion matrix, without normalization')

    print(cm)

    plt.figure(figsize=(10,8))
    plt.imshow(cm, interpolation='nearest', cmap=cmap)
    plt.title(title)
    plt.colorbar()
    tick_marks = np.arange(len(classes))
    plt.xticks(tick_marks, classes, rotation=45)
    plt.yticks(tick_marks, classes)

    fmt = '.2f' if normalize else 'd'
    thresh = cm.max() / 2.
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        plt.text(j, i, format(cm[i, j], fmt),
                 horizontalalignment="center",
                 color="white" if cm[i, j] > thresh else "black")

    plt.tight_layout()
    plt.ylabel('True label')
    plt.xlabel('Predicted label')

    plt.show()

    return