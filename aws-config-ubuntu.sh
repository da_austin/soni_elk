#!/bin/bash

clear

echo ""
echo "Starting Configuration Script (must be in /sonitrol/jake/ folder)...."
echo "!!!! NOTE !!!!! Be sure you have cloned the git repo to this node...."
echo ""

# CHECK DOCKER INSTALLATION
if command -v docker; then
	echo "-- Docker already installed, continuing...."
else
	echo "-- Installing Docker CE for Ubuntu...."

	sudo apt-get update

	# Install apt packages for https
	sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    software-properties-common

    # Get Docker Official GPG Key
    curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

    # Set up stable repository with apt
    sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"

   sudo apt-get update
   sudo apt-get install -y docker-ce

   sudo docker run hello-world
fi

# CHECK DOCKER-COMPOSE INSTALLATION
if command -v docker-compose; then
  echo "-- Docker Compose Already Installed, continuing....."
else
  # Download Latest Version of Docker Compose
  sudo curl -L https://github.com/docker/compose/releases/download/1.20.1/docker-compose-$(uname -s)-$(uname -m) -o /usr/local/bin/docker-compose

  # Apply Permissions to Binary
  sudo chmod +x /usr/local/bin/docker-compose

  # Check install
  docker-compose --version
fi

# BUILD IMAGES
echo ""
echo "Building Local Docker Images..."
echo ""

echo "-Listener"
sudo docker build -t sonitrol/listener:1.2 -f docker/Dockerfile.listener .

echo ""
echo "-Generating Local Demo Directory"
#sudo mkdir ~/demo/

#echo ""
echo "Configuration Finished, Launching Services."
sudo docker-compose -f docker/docker-compose-listener.yml up

