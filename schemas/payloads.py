"""
Note - the code doesn't use this. It was originally for
payload validation when we were building a REST APIs. Ignore
for now.
"""

wave_schema = {
	"type":"object",
	"properties": {
		"rate":{"type":"number"},
		"wave":{"type":"array"}
	},
	"required":["rate","wave"]
}