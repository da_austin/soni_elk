"""
AUTHOR: Jake Tutmaher
MAINTAINER: Stanley Black & Decker
LAST UPDATED: 3-23-18

Methods for general loading and manipulation of audio/sonitrol data
"""

import pandas as pd
import numpy as np
from utils import params

from scipy.io import wavfile

from models.research.audioset.vggish_input  import wavfile_to_examples
import models.research.audioset.vggish_params as vggish_params

def generate_mel_grams(wavfiles,labelfiles,fout,pds=True,window_length=params.WINDOW_LENGTH,
    hop_length=params.HOP_LENGTH,window_seconds=params.WINDOW_SECONDS,
    hop_seconds=params.HOP_SECONDS,sample_rate=params.SAMPLE_RATE):
    """
    Method for generating mel spectrograms from wavfiles/labels. These can be saved
    off as numpy arrays or pandas dataframe. Intended to be used with fine-tuned
    VGGish training.

    :param: wavfiles - str of raw WAV files to be encoded (must have 16-bit encoding)
    :param: labelfiles - str of excel files containing the associated labels
    :param: fout - str of directory and filename to save out to 
    :param: pd - bool if should be stored as pd dataframe of np array
    """

    #SET PARAMETERS
    vggish_params.STFT_WINDOW_LENGTH_SECONDS = window_length
    vggish_params.STFT_HOP_LENGTH_SECONDS = hop_length

    vggish_params.EXAMPLE_WINDOW_SECONDS = window_seconds
    vggish_params.EXAMPLE_HOP_SECONDS = hop_seconds
    vggish_params.SAMPLE_RATE=sample_rate

    print "-- Generating Mel Grams with the Following Settings --"
    print "STFT Window Length: "+str(window_length)
    print "STFT Window Seconds: "+str(window_seconds)
    print "Hop Length: "+str(hop_length)
    print "Hop Seconds: "+str(hop_seconds)
    print "Sample Rate: "+str(sample_rate) 

    # LOAD FILES AND GEN MEL GRAMS
    df_in = pd.concat((pd.read_excel(f) for f in labelfiles))
    mgrams = {}
    labels = {}
    for wfile in wavfiles:
        name = wfile.split("/")[-1]
        try:
            label = int(np.unique(df_in[df_in['file_name']==name]['num_label']))
        except:
            print "SKIPPING - FOUND MULTIPLE LABELS FOR THIS ENTRY"
            continue
        try:
            mel_temp = wavfile_to_examples(wfile)
        except ValueError:
            print "ValueError - Skipping "+wfile
            continue
        if mel_temp.shape!=(1,96,64):
            print "Error - Incorrect File Shape, Skipping "+str(mel_temp.shape)
            continue
        else:
            mgrams[wfile] = mel_temp 
            labels[wfile] = label*np.ones(mel_temp.shape[0])
    if pds:
        names = {key:params.orig_key[labels[key]] for key in labels}
        df_out = pd.DataFrame.from_dict(mgrams,orient='index')
        df_out['labels']=pd.Series(labels)
        df_out['names']=pd.Series(names)
        df_out.to_pickle(fout)
        return df_out
    else:
        keys = sorted([x for x in mgrams])
        data_vec = np.concatenate([mgrams[x] for x in keys],axis=0)
        label_vec = np.concatenate([labels[x] for x in keys],axis=0)
        np.savez_compressed(fout,data=data_vec,labels=label_vec)
        return data_vec,label_vec

