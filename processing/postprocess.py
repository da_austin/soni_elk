"""
AUTHOR: Jake Tutmaher
MAINTAINER: Stanley Black & Decker
LAST UPDATED: 3-23-18

Methods for postprocessing data
"""

import pandas as pd
import numpy as np
from utils import params


def gen_csv(data,labels,outfile='./output/sonitrol_16bit_binary.csv'):
    """
    Method for generating CSV data from embedding vectors
    :param: data - np array nrecords x nfeatures for each audio file
    :param: labels - list of labels (must be binary 0/1!!!)
    :param: outfile - csv filename to save out to
    """
    df_check = pd.DataFrame(data)
    labs_text_binary = ['indistinguishable' if labels[x]<1 else 'distinguishable' for x in range(len(labels))]
    df_check['label'] = pd.Series(labs_text_binary,index=df_check.index)
    df_check.to_csv(outfile)
    return

def format_labels_binary(labels,class_set):
    """
    Method for converting raw numberical labels (1-8,101-115)
    to binary 0/1 labels.
    
    :param: labels - list of raw labels
    :param: class_set - set of raw labels defined as '0' or indist.
    :return: list of labels in same order containing (0,1)
    """
    return [0 if labels[x] in class_set else 1 for x in range(len(labels))]

def load_np_data(npz_file,norm=True):
    """
    :param: npz_file - file path to npz file containing
            data and labels 
    :param: norm - Boolean value specifying if it should
            return normalized data. Assumes nrecords is
            along axis 0
    :return: data,label arrays [nrecords,nfeatures]
    """
    arr = np.load(npz_file)
    if norm:
        data,labs = arr['data'],arr['labels']
        ndata = (data-np.mean(data,axis=0))/np.std(data)
        return ndata,labs
    else:
        return arr['data'],arr['labels']
    
def load_pd_data(pckl_file,norm=True,nfeatures=640):
    """
    Method to load a pickle file and return a dataframe
    and a normalized or unnormalized np data array
    (nrecords x nfeatures)
    
    :param: pckl_file - Path to pickle file to load
    :param: norm - Boolean value for normalization
    :param: nfeatures - Length of feature vector 
            (default 640 for embedding length)
    :return: df - dataframe of pickle data
    :return: data - normalized or unnormalized np array
            (nrecords x nfeatures)
    """
    df = pd.read_pickle(pckl_file)
    data = df.iloc[:,:nfeatures].get_values()
    if norm:
        return df,(data-np.mean(data,axis=0))/np.std(data)
    else:
        return df,data

def vectorize(data_dict,labels_dict):
    """
    Method for vectorizing dictionary data (i.e. convert for dict to 
    np array). This was originally created to guarantee order within 
    the array.
    :param: data_dict - dictionary of key (audio file name) and value
        (array of embedding data 1x640)
    :param: labels_dict - dictionary of key (audio file name) and num
            label (1-8,101-115)
    :return: data_vec,label_vec - np array (nrecords x nfeatures) of
            embedding data and list of corresponding numerical data
    """
    keys = sorted([x for x in data_dict])
    data_vec = np.stack([data_dict[x] for x in keys],axis=0)
    label_vec = [labels_dict[x] for x in keys]
    return data_vec,label_vec

def panderize(data_dict,labels_dict,names=True):
    """
    Method for converting dictionary data to pandas DataFrames. Appends
    numeric class and string label as well. 
    
    :param: data_dict - dictionary object with audio file name and assoc.
            embedding vector
    :param: labels_dict - dictionary object with audio file name and ass-
            oc numeric label
    """
    df = pd.DataFrame.from_dict(data_dict,orient='index')
    df['labels']=pd.Series(labels_dict)
    if names:
        names = {key:params.orig_key[labels_dict[key]] for key in labels_dict}
        df['names']=pd.Series(names)
    return df