import sys
sys.path.append("..")
import glob
import numpy as np
from utils import preprocess
from utils import params
from utils import tf_vgg
from sklearn.model_selection import StratifiedKFold


def convert_labels(labs):

	background = params.background_set
	nlabels = [0 if params.orig_key[x] in background else 1 for x in list(labs)]
	nlabels_oh = np.zeros((len(nlabels), 2))
	nlabels_oh[np.arange(len(nlabels)), nlabels] = 1
	return nlabels_oh


def train_and_eval(dsave=params.MELGRAM_BINARY,dload=params.MELGRAM_BINARY,from_scratch=False,folds=10):

	if dload and not from_scratch:
		print "- Loading Training Data from "+dload
		arr = np.load(dload)
		data,labels = arr['data'],arr['labels']
	else:
		print "- Loading Training Data from Source Files"
		wav_files = params.wavfiles
		label_files = params.labfiles
		data,labels = preprocess.generate_mel_grams(wav_files,label_files,dsave,pds=False)
	
	skf = StratifiedKFold(n_splits=folds, shuffle=True)
	tot_loss = []
	tot_acc = []
	for train_idx,test_idx in skf.split(data,labels):
		X_train,y_train,X_test,y_test = data[train_idx],np.array(labels)[train_idx],data[test_idx],np.array(labels)[test_idx]
		y_train = convert_labels(y_train)
		y_test = convert_labels(y_test)

		print "---- Retraining Model ----"
		print "- Train Size: "+str(X_train.shape)+','+str(y_train.shape)
		tf_vgg.retrain(X_train,y_train,nepochs=5)

		print "---- Evaluating Model ----"
		print "- Test Size: "+str(X_test.shape)+','+str(y_test.shape)
		loss,acc = tf_vgg.evaluate(X_test,y_test)
		print "Test Stats || "+" Accuracy: "+str(acc)+", Log Loss: "+str(loss)
		tot_loss.append(loss)
		tot_acc.append(acc)

def eval(dload,checkpoint):
	arr = np.load(dload)
	data,labels = arr['data'],arr['labels']
	vlabels = convert_labels(labels)
	tf_vgg.eval(data[:32],vlabels[:32],ckpt=checkpoint)

if __name__=="__main__":
	train_and_eval(dsave="./output/numpy/melgrams_reduced.npz",dload="./output/numpy/melgrams_reduced.npz",from_scratch=False)
	#eval("./output/numpy/melgrams_reduced.npz",params.VGG_CHECKPOINT_SAVE)