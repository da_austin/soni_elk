# Sonitrol - ML Process

## Installation
### Local Install
To run this code locally, first clone the repository:
```
git clone https://<username>@bitbucket.org/sbdDigitalAccelerator/sonitrol.git
```

And make checkpoint and output directories if they don't already exist:
```
mkdir /sonitrol/jake/checkpoint/
mkdir /sonitrol/jake/checkpoitn/skl/
```
```
mkdir /sonitrol/jake/output/
mkdir /sonitrol/jake/output/csv/
mkdir /sonitrol/jake/output/pandas/
mkdir /sonitrol/jake/output/numpy/
```

Then install the dependencies (from sonitrol/jake):
```
pip install -r requirements.txt
```

Launch either the code with python 2.7 or the notebooks with jupyter. 

## Directory Structure

The sonitrol directory structure should look something like this once cloned:

![](/jake/img/directory-1.png)  

The only differences here would be the data/ directory, which does not exist in the bit-bucket repository. This directory is a proxy for the sonitrol audio (.wav files) from SonIP_Temp. You can either mount this directory to a data/ directory in this location, or copy the audio files to the local data/ directory in this location. 

A few things to note:

- ** The training code is made to read data from this directory. If you do not place the data in this directory locally, then you will need to modify the VGGFeatureExtract, MLModels, and Evaluate notebooks described below to find this data. Specifically, the utils/params.py (wavfiles and labfiles properties).**

- ** The training code is built to run on 16-bit encoded audio files. This will need to be changed to support other encodings (in-progress). Note, in production we can handle 16 bit and float32 encoded wavs and mp3s.**

The following bullets outline the directory structure within sonitrol/jake (which, for the purposes of this README is where training will happen)

 1. **backup:** backup files for custom kibana dashboard. once everything is launched on AWS, load in the preconfigured dashboard (export-kibana-ui-6.2.json) at the kibana endpoint following the instructions here: [Import Dashboard](https://www.elastic.co/guide/en/kibana/current/loading-a-saved-dashboard.html)

 2. **deprecated:** Folder holding deprecated code. This should largely be ignored. 
 
 3. **docker:** Folder holding configuration files for docker images. The most important file here is Dockerfile.listener (which contains instructions for building the sonitrol-listener docker images) and the docker-compose-listener.yml, which sets launch/deployment configurations for the docker image.
 
 4. **elasticsearch:** Folder holding config files for Elasticsearch. Assuming you're using 6.2 on AWS, the important files to track are es-mapping-6.2.json (schema information for storing inferencing/meta data)
 
 5. **img:** Folder holding images for parent README. Can largely be ignored for now. 
 
 6. **models:** Folder holding audioset tensorflow model. Cloned from (https://github.com/tensorflow/models)
 
 7. **notebooks:** Folder holding jupyter notebooks which process audio clips and train DL/ML models. 
 
 8. **processing:** Folder holding preprocess and postprocess scripts. These scripts act as glue between the VGG feature extraction step and the SKL classification step. See section below for more details.
 
 9. **production:** Folder holding final 'best model' weights for SKL classifier. These weights are copied into the docker image and deployed to an EC2 instance during production. 
 
 10. **resources:** Folder holding csv files with geocode and facility info (this was pulled from SQL database)
 
 11. **schemas:** Folder holding schemas of REST payloads - largely deprecated may be removed soon.
 
 12. **train:** contains modules for training SKL models and TF models using stratefied k-fold.
 
 13. **utils:** contains various models holding environment variables and runtime properties. Import file includes production-params.py, which holds all production environment variables for the sonitrol-listener.
 
 14. **visuals:** modules for plotting audio data, including model statistics (accuracy, recall, etc) and model evaluation such as confusion matrices. 

### Docker Install
You can also run this code by mounted a docker container. First clone the repository:
```
git clone https://jtutmaher@bitbucket.org/sbdDigitalAccelerator/sonitrol.git
```

Then install docker (via the following installation instructions [Install Docker](https://docs.docker.com/install/)).

and run the latest tensorflow image by mounting it to you local sonitrol directory
```
docker run -it -p 8888:8888 -v <path-to-sonitrol-directory>:/workspaces -v <path-to-mounted-data>:/workspaces/data --name tf_container tensorflow/tensorflow:latest /bin/bash
```

navigate to /workspaces/jake/, and install the docker reduced dependencies:
```
pip install -r docker-reduced-requirements.txt
```

Finally, clone the VGG Audioset TF code and configure it as a module [Tensorflow Research Audioset](https://github.com/tensorflow/models/tree/master/research/audioset)

Launch jupyter notebook and navigate to localhost:8888 on your machine:
```
jupyter notebook --allow-root
```

You should be able to run all of the scripts and access your local data.

## Training Process (jake/notebooks/)
### Overview
The training process is a 3-step process:

1. VGGFeatureExtract.ipynb: Load .wav training data, generate mel spectrograms, generate embedding vectors for each 5s sample. 

2. MLModels.ipynb: Train SKL ML models using VGG embedding vectors. Classify into an arbitrary number of classes.

3. Evaluate.ipynb: Evaluate the trained models on a held out test set. Look at recall and threshold sensitivities. 

### VGGFeatureExtract
This notebook is created to load in raw audio files, and run them through the pre-trained Tensoflow model to generate a 5x128 embedding vector (which when flattened becomes 1x640). First, run the dependencies:
```
import sys
sys.path.append("..")
import glob

import random
import numpy as np

from utils import params
from processing import postprocess
from train import tf_vgg
```

The params, postprocess, and tf_vgg are custom python code contained in the utils, processing, and train directories respectively (located in the sonitrol/jake parent folder).

Next, import the wave files and label excel files
```
# LOCATE RELAVENT DATA
wave_files = params.wavfiles
label_files = params.labfiles
```

The files are specified in the params.py file, and are loaded from a data directory mounted in the sonitrol repo (locally). Next, specify a temporary directory to store the train/test split for the files, as well as the raw embeddings as pickle files. In this case, I store everything in either a checkpoint folder (one level up), or a output folder. 
```
# SAVE RELEVENT DATA
tsave = "../checkpoint/train_test_split_04112018.npz"
fsave_train = "../output/pandas/embeddings_16bit_train_04112018.pickle"
fsave_test = "../output/pandas/embeddings_16bit_test_04112018.pickle"
```

Then, execute a the train/test split on your data and save the output
```
# TRAIN TEST SPLIT
tsize = int(0.8*len(wave_files))
random.shuffle(wave_files)
train_files,test_files = wave_files[:tsize],wave_files[tsize:]
print len(train_files),len(test_files)

# SAVE FILES - ONLY RUN ONCE
np.savez_compressed(tsave,train_files=train_files,test_files=test_files)
```

Finally, extract the features and save those off as pickle files (for eventually SKL classification)
```
#EXTRACT TRAIN FEATURES
preds,labs = tf_vgg.feature_extract(train_files,label_files,aug=False)
df = postprocess.panderize(preds,labs)
df.to_pickle(fsave)
```

Don't forget to do the same to the test files:
```
#EXTRACT TEST FEATURES
wfiles = np.load(tsave)
preds,labs = tf_vgg.feature_extract_hostile(wfiles['test_files'],params.labfiles,aug=False)
df = postprocess.panderize(preds,labs)
df.to_pickle(fsave)
```

### MLModels
This notebook is intended to be run *after* the VGGFeatureExtract notebook. First import the relevant modules

```
import sys
sys.path.append("..")
import numpy as np

from utils import params
from processing import postprocess
from train import skl_train
import visuals.plot as cplt

from sklearn.ensemble import RandomForestClassifier
from sklearn.ensemble import AdaBoostClassifier
from sklearn.neural_network import MLPClassifier
import xgboost as xgb
from sklearn.externals import joblib
```

Note that the utils, processing, train and visuals are all custom modules in the repository. Next, load the training data and plot the class distribution
```
# LOAD DATA FRAME
dfile = "../output/pandas/embeddings_16bit_aug_train_v3.pickle"
df_train,data = postprocess.load_pd_data(dfile,norm=False,nfeatures=640)

# GET LABELS
labels = df_train['labels'].get_values()

print "Train Size: "+str(len(labels))
print "-- All Labels"
cplt.plot_labels(labels)
```

Then, relabel the data to match a specific sound hierarchy (in this case we're sticking with the indistinguishable/distinguishable breakdown).
```
# CREATE BINARY LABELS
orig = set([1,2,3,4,5,6,7,8])
blabels = [0 if x in orig else 1 for x in labels]

# NORAMLIZE DATA
train_mean = np.mean(data,axis=0)
train_std = np.std(data)
X_train = (data-train_mean)/train_std
y_train = blabels

print "Class Ratio || Indist.: "+str(np.count_nonzero(np.array(y_train)==0)/float(len(blabels)))+", Dist: "+str(np.count_nonzero(y_train)/float(len(blabels)))
print "-- Binary Labels"
cplt.plot_labels(blabels,key={0:'indistinguishable',1:'distinguishable'})
```

Finally, train and save the SKL model (in this case we're training a Random Forest, MLP, and a XGBoost classifier)
```
# TRAIN AND VALIDATE MODELS
# MLP
print "-- Training MLP"
clf_mlp = MLPClassifier(hidden_layer_sizes=(100,100,100,100),
                        batch_size=32)
mlp_metrics = skl_train.train_and_eval(X_train,y_train,clf_mlp)

# XGBOOST
print "-- Training XGBoost"
clf_xgb = xgb.XGBClassifier(max_depth=9,
                            n_estimators=1800,
                            min_child_weight=1,
                            learning_rate=0.04,
                            subsample=0.8,
                            colsample_bytree=0.6,
                            gamma= 2)
xgb_metrics = skl_train.train_and_eval(X_train,y_train,clf_xgb,xgb=True,folds=10)

# RANDOM FOREST
print "-- Training Random Forest"
clf_rf = RandomForestClassifier(n_estimators=1800,
                                max_depth=9)
rf_metrics = skl_train.train_and_eval(X_train,y_train,clf_rf)
```

```
# SAVE FILE AND DATA
mfile1 = "../checkpoint/skl/mlp_binary_v2.sav"
mfile2 = "../checkpoint/skl/rf_binary_v2.sav"
mfile3 = "../checkpoint/skl/xgb_binary_v2.sav"
sfile = "../checkpoint/skl/train_stats_binary_v2.npz"

joblib.dump(clf_mlp, mfile1)
joblib.dump(clf_rf, mfile2)
joblib.dump(clf_xgb, mfile3)
np.savez_compressed(sfile,train_mean=train_mean,train_std=train_std,breakdown=list(orig))
```

The notebook also includes a few blocks for viewing the results from the classifiers.

### Evaluate

Import the required modules (again, utils, processing, train, and visuals are all custom modules in the repo):
```
import sys
sys.path.append("..")

import numpy as np

from utils import params
from processing import postprocess
from train import skl_train
import visuals.plot as cplt

from sklearn.metrics import accuracy_score,recall_score,confusion_matrix
from sklearn.preprocessing import binarize
from sklearn.externals import joblib

import matplotlib.pyplot as plt
```

Next, load the test data (the embedding vectors you generated from the test files in the VGGFeatureExtract step):
```
# LOAD TEST EMBEDDING DATA
dfile = "../output/pandas/embeddings_16bit_aug_test_v3.pickle"
df_test,tdata = postprocess.load_pd_data(dfile,norm=False,nfeatures=640)

# PLOT DATA
print "Test Size: "+str(len(tdata))
print "-- All Values: "
cplt.plot_labels(df_test['labels'].get_values())
```
This will also generate a plot of the class distribution.

Then convert the labels based on the class hierarchy/breakdown established in the MLModels step:
```
# LOAD TRAIN STATISTICS
#sfile = "./checkpoint/skl/train_stats_v3.npz"
sfile = "../checkpoint/skl/train_stats_binary_v2.npz"
train_stats = np.load(sfile)

# CONVERT TO BINARY LABELS
orig = set(train_stats['breakdown'])
y_test = [0 if x in orig else 1 for x in df_test['labels'].get_values()]
#y_test = [1 if x==108 else 0 for x in df_test['labels'].get_values()]

print "Class Ratio || Indist.: "+str(np.count_nonzero(np.array(y_test)==0)/float(len(y_test)))+", Dist: "+str(np.count_nonzero(y_test)/float(len(y_test)))
print "-- Binary Values"
cplt.plot_labels(y_test,key={0:'indistinguishable',1:'distinguishable'})
```
This will also generate a plot of the new class distribution.

Next, load the trained model and normalize the test data (w.r.t. the training data):
```
# LOAD TRAINED MODEL
mfile = "../checkpoint/skl/mlp_binary_v2.sav"
trained_model = joblib.load(mfile)

# NORAMLIZE DATA
X_test = (tdata-train_stats['train_mean'])/train_stats['train_std']
```

Finally, score the test data against the trained model and generate some scoring metrics:
```
# PREDICT TEST CLASSES
thresh=0.15
y_proba = trained_model.predict_proba(X_test)
y_proba_pred = binarize(y_proba,thresh)[:,1]

# PLOT CONFUSION MATRIX
cm_res = confusion_matrix(y_test,y_proba_pred)
print "Accuracy: "+str(accuracy_score(y_test,y_proba_pred))
print "Recall: "+str(recall_score(y_test,y_proba_pred))
print "Not Hostile Screened: "+str(cm_res[0,0]/float(cm_res[0,0]+cm_res[0,1]))
print "Unknown Missed: "+str(cm_res[1,0]/float(cm_res[1,0]+cm_res[1,1]))
cplt.plot_confusion_matrix(cm_res,['indist','dist'])

```
You can modify the threshold and play around with the numbers/scores. Below this code block is a few blocks that outline the class breakdown of the false positives and false negatives. This can be interesting/telling regarding which classes your models have trouble with. 