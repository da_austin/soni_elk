#!/usr/bin/python

"""
sonitrol-listener.py: Python script to listen to a specific directory,
identity recently updated files (mp3/wav), and inference those files
using a pretrained VGGish model combined with a downstream XGBoost
SKL model. 

This script rights these results, along with any corresponding meta-
data, to an elasticsearch index. This index + data can be visualized
using Kibana, helping monitor not-hostile/unknown events in near-real
time. 

This script is also intended to be dockerized. Please refer to the 
docker fold for more detail - or the README.md
"""

__author__ = "Jacob Tutmaher"
__copyright__ = "Copyright 2018, Stanley Black & Decker"
__date__ = "04/09/2018"

__version__ = "1.0"
__maintainer__ = "Stanley Black & Decker | Robert Coop"
__email__ = "robert.coop@sbdinc.com"
__status__ = "Prototype"

import glob
import time
import datetime
import json

import numpy as np
import pandas as pd
from scipy.io import wavfile
import logging
from colorlog import ColoredFormatter
from pydub import AudioSegment

from utils import production_params as params
from excpts.sonitrol import FileTooShortException 

import elasticsearch
from elasticsearch import Elasticsearch

import tensorflow as tf
import models.research.audioset.vggish_params as vggish_params
import models.research.audioset.vggish_slim as vggish_slim
from models.research.audioset.vggish_input import waveform_to_examples

from sklearn.externals import joblib
from sklearn.preprocessing import binarize


def _setup_logger():
    """
    Logger for python monitoring service

    :param: log_level - logging string of level
    :param: log_format - format of logger
    :return: logger
    """
    logging.root.setLevel(log_level)
    formatter = ColoredFormatter(log_format)
    stream = logging.StreamHandler()
    stream.setLevel(log_level)
    stream.setFormatter(formatter)

    logger = logging.getLogger('pythonConfig')
    logger.setLevel(log_level)
    logger.addHandler(stream)
    return logger
    
def _gen_mel_gram(wfile):
    """
    Generates Mel Spectrogram using VGGish melgram function. Converts

    :param: wfile - string of mp3 or wav file to read in and generate
            mel gram from
    :return: mel_gram - (5x96x64) mel spectrogram (assumes 5 second samples)
    """ 
    # WAVEFILES 
    if wfile.endswith(".wav"):
        rate, wave = wavfile.read(wfile)
        duration = len(wave)/float(rate)
        # CHECK THAT WAVE IS 5 SECONDS
        if duration>5.0:
            logger.warn("-Mel Gram: File Too Long, Truncating to First 5 Secs...")
            wave = wave[:int(5*rate)]
        elif duration<5.0:
            raise FileTooShortException("File Shorter than 5.0 seconds: "+str(wfile)+" | Length: "+str(duration))     
        # GENERATE MEL GRAM
        if wave.dtype==np.float32:
            logger.debug("-Mel Gram: File is float32 wave: "+str(wfile))
            return waveform_to_examples(wave / 2.0, rate)
        elif wave.dtype==np.int16:
            logger.debug("-Mel Gram: File is a int16 wave: "+str(wfile))
            return waveform_to_examples(wave / 32768.0,rate)
        else:
            raise Exception("Error - audio encoding not recognized for wave of type: "+str(wave.dtype))
    # MP3s
    elif wfile.endswith(".mp3"):
        logger.debug("-Mel Gram: File is an mp3: "+str(wfile))
        sound = AudioSegment.from_mp3(wfile)
        rate,wave = sound.frame_rate,np.array(sound.get_array_of_samples(), dtype=np.int16)
        duration = len(wave)/float(rate)
        if duration>5.0:
            logger.warn("-Mel Gram: File Too Long, Truncating to First 5 Secs...")
            wave = wave[:int(5*rate)]
        elif duration<5.0:
            raise FileTooShortException("File Shorter than 5.0 seconds: "+str(wfile)+" | Length: "+str(duration)) 
        return waveform_to_examples(wave / 32768.0,rate)
    else:
        raise Exception("Error - file extensions not mp3 or wav for file: "+str(wfile))


def _gen_embedding(mel_gram):
    """
    Generates embedding vector from pre-trained VGGish model

    :param: mel_gram - np.array of shape (5x94x64) of mel spectrogram
            of sound
    :return: embedding_batch - 1x640 np.array
    """

    logger.debug("- VGG: Asserting Mel Shape is (5,96,64) for Mel Gram of Shape: " + str(mel_gram.shape))
    assert mel_gram.shape == (5, 96, 64)

    # INFERENCE MEL GRAM
    logger.debug("- VGG: Extracting Embedding Vector")
    [embedding_batch] = sess.run([embedding_tensor], feed_dict={features_tensor: mel_gram})

    return np.array(embedding_batch).flatten()


def _gen_skl_proba(embedding):
    """
    Generates skl raw probability predictions from pretrained model

    DO WE WANT TO INCLUDE A ML VERSION

    :param: embedding - np.array of 1x640 element embedding from pretrain VGGish
    :return: list - [nclasses] of floats represented predicted probabilities
    """

    # NORMALIZE DATA
    logger.debug("- SKL: Normalizing Embedding Vector")
    norm = (embedding - train_stats['train_mean']) / train_stats['train_std']

    # LOAD MODEL AND PREDICT
    logger.debug("- SKL: Generating Predictions from Embedding Vector")
    return skl_model.predict_proba(np.expand_dims(norm, axis=0))


def _check_dir(dmonitor,start_files=set([])):
    """
    Method to determine the number of new files added to a monitored directory.

    :param: start_files - set of files found at startup. these do not get loaded.
    :return: curr_files - current files in directory, only a list of differences
                        between start up and curr is returned.
    """
    curr_files = set(glob.glob(dmonitor+"*"))
    if curr_files == start_files:
        return None
    else:
        return list(curr_files - start_files)


def _format_datetime(dstr, e='%b_%d_%Y-%H_%M_%S'):
    """
    Audio file date times are not ISO compliant. This method parses the datetime
    and converts it to an ISO compliant format.

    SHOULD ADD TIME ZONE TO MATCH CENTRAL TIME

    :param: dstr - datetime string
    :param: e - expected format of string
    :return: d - isoformated date string
    """
    d = datetime.datetime.strptime(dstr, e)
    return d.isoformat()


def _get_meta_data(wfile, meta={}):
    """
    Parse and retrieve meta data file from audio file name and
    geocode resource.

    :param: wfile - string of audio file tag
    :param: meta - dictionary of meta data for file, default is null
    :return: meta - dict of metadata from parse filename and geocode
            resource.
    """

    # TODO SQL database read metadata

    # PARSE FILE INFO
    audio_tag = wfile.split('/')[-1]
    meta['audio_tag'] = audio_tag
    str_list = audio_tag.split('-')
    meta['display_num'] = int(str_list[1].split('_')[0])
    meta['display_grp'] = int(str_list[1].split('_')[1])
    tstr = str_list[2] + '-' + str_list[3].split('.')[0]
    meta['time_stamp'] = _format_datetime(tstr)

    # GET FACILITY INFO
    facility = df_address[df_address['DisplayNumber'] == meta['display_num']].iloc[0]
    meta['facility_name'] = facility['Name']
    meta['facility_address'] = facility['Address']
    meta['facility_address2'] = str(facility['Address2'])
    meta['city'] = facility['City']
    meta['state'] = facility['State']
    meta['zip'] = str(facility['Zip']).rstrip()
    meta['location'] = [float(facility['lon']), float(facility['lat'])]

    return meta


def predict(wfile):
    """
    Generates predictions using the pre-trained VGGish TF model
    and the pre-trained SKL models (specifically XGBoost). These
    are loaded into memory from local directories during prod.

    Predictions for new files are written to a index in ES

    :param: wfile - string path to wavfile to read and generate
                    predictions on
    """

    #TODO Check file name, support WAV or MP3 file
    """
    Support MP3 via pydub:
    
    from pydub import AudioSegment
    
    sound = AudioSegment.from_mp3(fname)
    
    samples = sound.get_array_of_samples()
    
    """
    try:
        # READ IN WAV FILE
        logger.info("Reading/Validating Wavefile & Generating Mel Gram")
        mel_gram = _gen_mel_gram(wfile)

        # GENERATE EMBEDDING
        logger.info("Generating Embedding Vector")
        embedding = _gen_embedding(mel_gram)

        # GENERATE PREDICTED PROBABILITIES
        logger.info("Generating Predictions")
        y_proba = _gen_skl_proba(embedding)

        # GET TIMESTAMP AND FACILITY ID

        # TODO - This metadata should come from SQL
        logger.info("Generating Metadata")
        res = _get_meta_data(wfile)

        # BINARIZE AND GENERATE RESPONSE
        predict = binarize(y_proba, skl_thresh)[:, 1]
        res['raw_preds'] = y_proba.flatten().tolist()
        res['class_num'] = int(predict)
        res['thresh'] = skl_thresh
        res['class_name'] = class_names[int(predict)]

        # STORE PREDICTIONS IN ES
        logger.info("SUCCESS! Storing Predictions In Elasticsearch")
        logger.debug("- Predictions: ")
        logger.debug(json.dumps(res))
        es_res = es_client.index(index=es_index, doc_type=es_doc, body=res)
        logger.debug(json.dumps(es_res))
    
    except FileTooShortException:
        logger.warn("FILE TOO SHORT, STORING AS POSITIVE PREDICTION...")
        # SET PREDICTION TO POSITIVE VALUE IF FILE TOO SHORT
        y_proba = [0.0,1.0]

        # GET METADATA
        logger.info("Generating Metadata")
        res = _get_meta_data(wfile)

        # GENERATE RESPONSE
        res['raw_preds'] = y_proba
        res['class_num'] = int(1.0)
        res['thresh'] = skl_thresh
        res['class_name'] = class_names[int(1.0)]

        # STORE PREDICTIONS IN ES
        logger.info("SUCCESS! Storing Predictions In Elasticsearch")
        logger.debug("- Predictions: ")
        logger.debug(json.dumps(res))
        es_res = es_client.index(index=es_index, doc_type=es_doc, body=res)
        logger.debug(json.dumps(es_res))
    
    return

def _get_current_sonip_subdir(basedir):
    """
    Method to get most recent SonIP directory via 
    datetime.

    :param: basedir - string of base directory where SonIP is mounted
    :return: basedir+todays date
    """
    def parse_times(ts):
        try:
            return datetime.datetime.strptime(ts, "%m-%d-%Y")
        except ValueError:
            return None
        
    dirs = glob.glob(basedir+"*")
    timestamps = [x.split('/')[-1] for x in dirs]
    dates = [parse_times(ts) for ts in timestamps]
    dates = [x for x in dates if x!=None]
    dates.sort()
    sorteddates = [datetime.datetime.strftime(ts, "%m-%d-%Y") for ts in dates]
    return basedir+sorteddates[-1]+"/"

def monitor():
    """
    Monitor files in the monitor dir (defined in main code block) for
    file additions. Will queue those updated files and run the prediction
    method on them.

    Right now, this method is on a timer - probably a better way to do this.
    """

    # TODO Change this to poll SQL server
    new_monitor_dir = _get_current_sonip_subdir(monitor_dir)
    sfiles = set(glob.glob(new_monitor_dir+"*"))
    while True:
        update_monitor_dir = _get_current_sonip_subdir(monitor_dir)
        if update_monitor_dir!=new_monitor_dir:
            new_monitor_dir = update_monitor_dir
            sfiles = set([])
        logger.info("MONITOR - Listening to Files at "+new_monitor_dir)
        try:
            updated_files = _check_dir(new_monitor_dir,start_files=sfiles)
            if updated_files:
                logger.info(str(len(updated_files)) + " new files found...")
                for nfile in updated_files:
                    predict(nfile)
                sfiles = sfiles.union(updated_files)
                time.sleep(monitor_rate)
            else:
                logger.info("No new files found, continuing....")
                time.sleep(monitor_rate)
        except Exception as e:
            logger.error("ERROR ANALYZING FILES - continuing....")
            logger.error(e)
            sfiles = sfiles.union(updated_files)
            continue
    return


if __name__ == "__main__":

    # PARAMETERS
    # - ML
    geocode_resource = params.GEOCODE_RESOURCE
    vgg_model = params.VGG_CHECKPOINT_MODEL
    skl_stats = params.SKL_CHECKPOINT_STATS
    skl_model_ckpt = params.SKL_CHECKPOINT_MODEL
    skl_thresh = params.SKL_THRESH
    class_names = params.CLASS_NAMES
    # - ES
    es_host = params.ES_HOST
    es_port = params.ES_PORT
    es_mapping = params.ES_MAPPING
    es_index = params.ES_INDEX
    es_doc = params.ES_DOC
    es_delay = params.ES_DELAY
    # - Data
    monitor_dir = params.MONITOR_DIR
    monitor_rate = params.MONITOR_RATE
    # - Logging
    log_format = params.LOG_FORMAT
    log_level = params.LOG_LEVEL

    # SETUP LOGGER
    logger = _setup_logger()
    logger.info("Sonitrol Listener Service Starting Up: ")
    logger.debug("-- Using the following parameters: ")
    logger.debug({"geocode_resource": geocode_resource, "vgg_model": vgg_model,
                  "skl_stats": skl_stats, "skl_model": skl_model_ckpt,
                  "skl_thresh": skl_thresh, "es_host": es_host, "es_port": es_port,
                  "class_names": class_names, "es_mapping": es_mapping, "es_index": es_index,
                  "es_doc": es_doc, "es_delay": es_delay, "montored_files": monitor_dir,
                  "monitor_rate": monitor_rate, "log_level": log_level})

    # SETUP TF SESSION
    logger.info("-- Setting Up Tensorflow VGGISH Model")
    tf.reset_default_graph()
    sess = tf.Session()

    # LOAD VGGISH
    vggish_slim.define_vggish_slim(training=False)
    vggish_slim.load_vggish_slim_checkpoint(sess, vgg_model)
    features_tensor = sess.graph.get_tensor_by_name(vggish_params.INPUT_TENSOR_NAME)
    embedding_tensor = sess.graph.get_tensor_by_name(vggish_params.OUTPUT_TENSOR_NAME)

    # LOAD ADDRESS LOOKUP TABLE
    df_address = pd.read_csv(geocode_resource)

    # LOAD SKL MODEL
    logger.info("-- Setting Up SKL Model")
    train_stats = np.load(skl_stats)
    skl_model = joblib.load(skl_model_ckpt)

    # SET UP ELASTICSEARCH CLIENT
    try:
        logger.info("Setting Up Elasticsearch Client: ")
        logger.info({'host': es_host, 'port': es_port})
        time.sleep(es_delay)
        es_client = Elasticsearch(hosts=[{'host': es_host, 'port': es_port}])
        mres = es_client.indices.create(index=es_index, body=json.load(open(es_mapping)),ignore=400)
    except elasticsearch.exceptions.ConnectionError as e:
        logger.error("Could Not Connect to Elasticsearch, this Service will not operate correctly")
        logger.error(e)
        logger.error(mres)
    except elasticsearch.exceptions.RequestError as e:
        logger.warn("Client encountered a request error when loading mapping, continuing with current index:")
        logger.warn(e)
        logger.debug("Index: " + es_index)
        logger.debug("Required Mapping: ")
        logger.debug(json.load(open(es_mapping)))

    # MONITOR DIRECTORY
    monitor()
