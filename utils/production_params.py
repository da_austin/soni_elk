"""
File containing label assignments for audio data
"""
import os
import logging

# LOGGER
LOG_LEVEL = logging.DEBUG
LOG_FORMAT = "  %(log_color)s%(levelname)-8s%(reset)s | %(log_color)s%(message)s%(reset)s"

# VGG SETTINGS
VGG_CHECKPOINT_MODEL = "./models/research/audioset/vggish_model.ckpt"

# SKL SETTINGS
SKL_CHECKPOINT_MODEL = "./production/xgb_binary_v2.sav"
SKL_CHECKPOINT_STATS = "./production/train_stats_binary_v2.npz"
SKL_THRESH = 0.5

# CLASS NAMES
CLASS_NAMES = {0: "Not Hostile",1:"Unknown"}
GEOCODE_RESOURCE = "./resources/displaynumber_address_latlon.csv"

# ELASTIC SEARCH
ES_HOST = os.environ.get("ELASTICSEARCH_HOST")
ES_PORT = os.environ.get("ELASTICSEARCH_PORT")
ES_MAPPING = "./elasticsearch/es-mapping-6.2.json"
ES_INDEX = "sonitrol"
ES_DOC = "test"
ES_DELAY = 0 #seconds

# -- LOCAL -- IN CONTAINER MONITORING DIRECTORY
MONITOR_DIR = "/workspaces/sonitrol/data/"
MONITOR_RATE = 3 #seconds 