"""
File containing label assignments for audio data
"""
import glob
import logging

# LOGGER
LOG_LEVEL = logging.DEBUG
LOGFORMAT = "  %(log_color)s%(levelname)-8s%(reset)s | %(log_color)s%(message)s%(reset)s"

# VGG SETTINGS
VGG_CHECKPOINT_LOAD = "../models/research/audioset/vggish_model.ckpt"
VGG_CHECKPOINT_SAVE = "./checkpoint/retrain_vgg.ckpt"

# SKL SETTINGS
SKL_CHECKPOINT_MODEL = "./checkpoint/skl/xgb_binary_v2.sav"
SKL_CHECKPOINT_STATS = "./checkpoint/skl/train_stats_binary_v2.npz"
SKL_THRESH = 0.5

# MELGRAM SETTINGS
MELGRAM_BINARY = "./output/numpy/melgrams.npz"
WINDOW_LENGTH = 0.025 #0.125 
WINDOW_SECONDS = 0.96 #4.8
HOP_LENGTH = 0.010 #0.050
HOP_SECONDS = 0.96 #4.8
SAMPLE_RATE = 16000

# DATA SETTINGS
wavfiles = glob.glob("../../data/sonitrol_audio*/*.wav")
labfiles = glob.glob("../../data/sonitrol_audio*/*.xlsx")
facility_wavfiles = glob.glob("../data/facility/*.wav")
facility_labfiles = glob.glob("../data/facility/*.xlsx")

# CLASS NAMES
class_names = {0: "Not Hostile",1:"Hostile"}

#LABEL SETTINGS
background_set = set(['static','white_noise','hvac','click','truck','weather','compressor'])
indist_set = set([1,2,3,4,5,6,7,8])
dist_set = set([101,102,103,104,105,106,107,108,109,110,111,112,113,114,115,119])
orig_key = {1:'static',
           2:'click',
           3:'hvac',
           4:'compressor',
           5:'thumps',
           6:'beep',
           7:'white_noise',
           8:'fax',
           101:'voices',
           102:'music',
           103:'alarm',
           104:'animal',
           105:'train',
           106:'phone',
           107:'glass',
           108:'hostile',
           109:'alarm',
           110:'truck',
           111:'siren',
           112:'buzzer',
           113:'answer_machine',
           114:'television',
           115:'weather',
           119:'unknown'}
new_key = {'static':1,
           'click':2,
           'hvac':3,
           'compressor':4,
           'thumps':5,
           'beep':6,
           'white_noise':7,
           'fax':8,
           'voices':9,
           'music':10,
           'alarm':11,
           'animal':12,
           'train':13,
           'phone':14,
           'glass':15,
           'hostile':16,
           'alarm':17,
           'truck':18,
           'siren':19,
           'buzzer':20,
           'answer_machine':21,
           'television':22,
           'weather':23,
          'unknown':24}
lee_key = {'static':0,
           'click':0,
           'hvac':0,
           'compressor':0,
           'thumps':0,
           'beep':3,
           'white_noise':0,
           'fax':3,
           'voices':1,
           'music':1,
           'alarm':5,
           'animal':4,
           'train':2,
           'phone':3,
           'glass':5,
           'hostile':4,
           'alarm':5,
           'truck':0,
           'siren':5,
           'buzzer':5,
           'answer_machine':3,
           'television':1,
           'weather':0,
          'unknown':0}