"""
AUTHOR: Jake Tutmaher
MAINTAINER: Stanley Black & Decker
LAST UPDATED: 3-23-18

Methods for utilizing pre-trained AudioSet VGGish Model
See TF documentation for more detail:
"""

from scipy.signal import butter
from scipy.signal import lfilter

def butter_bandpass(lowcut, highcut, fs, order=5):
    nyq = 0.5 * fs
    low = lowcut / nyq
    high = highcut / nyq
    b, a = butter(order, [low, high], btype='band')
    return b, a


def butter_bandpass_filter(data, lowcut, highcut, fs, order=5):
    b, a = butter_bandpass(lowcut, highcut, fs, order=order)
    y = lfilter(b, a, data)
    return y
