import numpy as np

from scipy.io import wavfile
from models.research.audioset.vggish_input  import wavfile_to_examples, waveform_to_examples
import librosa

def white_noise(wfile,scale=0.5):
    """
    Method to add white noise to the wave. Reads in the .wav file
    using scipy, adds random noise based on the relative mean and
    std of the wave, returns the mel spectrogram - to be used for
    feature extraction.

    :param: wfile - str local path to .wav file to 
    :param: scale (optional) - float between 0 and 1 for ampl. of
            white noise
    :return: mel_spectrogram - np.array (5x96x64) of mel spectrogr
            am to be returned to TF inferencer. 
    """
    # Load wave
    rate,wave = wavfile.read(wfile)
    assert wave.dtype == np.int16, 'Bad sample type: %r' % wav_data.dtype
    # Adding white noise 
    noise = np.random.normal(np.mean(wave),scale*np.std(wave),len(wave))
    # Normalizing
    dwave = wave+noise
    dwave[dwave>32768.0]=32768.0
    dwave[dwave<-32768.0]=-32768.0
    swave = dwave / 32768.0
    return waveform_to_examples(swave,rate)

# This is deprecated - will be removed in future versions
def roll_data(data):
    roll = np.random.randint(0,high=len(data),size=1)
    return np.roll(data,roll)

def stretch_data(data):
    """
    Method to stretch waveform. To be updated to reflect different 
    inputs similar to white_noise above
    """
    factor = np.random.uniform(0,2,1)
    input_length = len(data)
    data = librosa.effects.time_stretch(data, factor)
    if len(data)>input_length:
        data = data[:input_length]
    else:
        data = np.pad(data, (0, max(0, input_length - len(data))), "constant")

    return data