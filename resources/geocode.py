import pandas as pd
import requests 
import json

df_address = pd.read_csv('./resources/displaynumber_address.csv')

geocode_api = 'http://saas.aws.api.here.com/geocoder/6.2/geocode.json?app_id=GhzvSh2fPmdIH7hh0tNh&app_code=00y8Rnf8KC8iLvszQwjLHg'

for row in range(len(df_address)):
	record = df_address.iloc[row]
	idx = df_address.index.get_values()[row]
	street = str(record['Address']).replace('#','').replace(' ','+')
	city = str(record['City']).replace(' ','+')
	state = str(record['State'])
	zipp = str(record['Zip']).replace(' ','+')
	query_str = street+'+'+city+'+'+state+'+'+zipp
	query_str.replace('++','+')
	endpoint = geocode_api+'&searchtext='+query_str
	res = requests.get(endpoint)
	res_json = res.json()
	print endpoint
	try:
		latitude = float(res_json['Response']['View'][0]['Result'][0]['Location']['MapView']['TopLeft']['Latitude'])
		longitude = float(res_json['Response']['View'][0]['Result'][0]['Location']['MapView']['TopLeft']['Longitude'])
	except IndexError:
		latitude = None
		longitude=None
	print idx,latitude,longitude
	df_address.set_value(idx,'lat',latitude)
	df_address.set_value(idx,'lon',longitude)

df_address.to_csv('./resources/displaynumber_address_latlon.csv')