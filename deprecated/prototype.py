from flask import Flask, request
from flasgger import Swagger, swag_from
import numpy as np
import logging
from colorlog import ColoredFormatter
import json
from schemas import payloads
from jsonschema import validate

from sklearn.externals import joblib
from sklearn.preprocessing import binarize
from scipy.io import wavfile 

from utils import params

import tensorflow as tf
import models.research.audioset.vggish_params as vggish_params
import models.research.audioset.vggish_slim as vggish_slim
from models.research.audioset.vggish_input  import waveform_to_examples

app = Flask(__name__)
swagger = Swagger(app)

def _gen_mel_gram(rwave,rate):
	logger.debug("- Mel Gram: Checking 16-bit Encoding")
	assert rwave.dtype == np.int16, 'Bad sample type: %r' % rwave.dtype
	logger.debug("- Mel Gram: Converting Wave To Mel Gram")
	return waveform_to_examples(rwave / 32768.0,rate)

def _gen_embedding(mel_gram):

	logger.debug("- VGG: Asserting Mel Shape is (5,96,64) for Mel Gram of Shape: " +str(mel_gram.shape))
	assert mel_gram.shape==(5,96,64)

	# INFERENCE MEL GRAM
	logger.debug("- VGG: Extracting Embedding Vector")
	[embedding_batch] = sess.run([embedding_tensor],feed_dict={features_tensor: mel_gram})

	return np.array(embedding_batch).flatten()

def _gen_skl_proba(embedding):

	# NORMALIZE DATA
	logger.debug("- SKL: Normalizing Embedding Vector")
	norm = (embedding-train_stats['train_mean'])/train_stats['train_std']

	# LOAD MODEL AND PREDICT
	logger.debug("- SKL: Generating Predictions from Embedding Vector")
	return skl_model.predict_proba(np.expand_dims(norm,axis=0))

def _setup_logger():
    LOGFORMAT = params.LOGFORMAT
    LOG_LEVEL = params.LOG_LEVEL

    logging.root.setLevel(LOG_LEVEL)
    formatter = ColoredFormatter(LOGFORMAT)
    stream = logging.StreamHandler()
    stream.setLevel(LOG_LEVEL)
    stream.setFormatter(formatter)

    logger = logging.getLogger('pythonConfig')
    logger.setLevel(LOG_LEVEL)
    logger.addHandler(stream)
    return logger

@app.route("/sonitrol/predict",methods=['POST'])
@swag_from("/schemas/test.yml")
def predict():

	try:
		logger.info("Reading/Validating Payload")
		payload = json.loads(request.get_data())
		validate(payload,payloads.wave_schema)
		rate,wave = float(payload["rate"]),np.array(payload["wave"]).astype('int16')

		# GENERATE MEL GRAMS
		logger.info("Generating Mel Gram")
		mel_gram = _gen_mel_gram(wave,rate)

		# GENERATE EMBEDDING
		logger.info("Generating Embedding Vector")
		embedding = _gen_embedding(mel_gram)

		# GENERATE PREDICTED PROBABILITIES
		logger.info("Generating Predictions")
		y_proba = _gen_skl_proba(embedding)

		# BINARIZE AND GENERATE RESPONSE
		predict = binarize(y_proba,params.SKL_THRESH)[:,1]
		res = {"raw_preds":y_proba.flatten().tolist(),"class_num":int(predict),"thresh":params.SKL_THRESH,"class_name":params.class_names[int(predict)]}
		logger.info("SUCCESS! Returning Predictions")
		logger.debug("- Predictions: "+str(res))
		return json.dumps(res)

	except Exception as e:
		logger.error("Error generating predictions:")
		raise e

@app.route("/sonitrol/test",methods=['GET'])
def test():
	logger.info("Successfully Hit Test Endpoint")
	res = "Sonitrol Application is Running"
	return res

if __name__=="__main__":
	# SETUP LOGGER
	logger = _setup_logger()

	# SETUP TF SESSION
	logger.info("-- Setting Up Tensorflow VGGISH Model")
	tf.reset_default_graph()
	sess = tf.Session()

	# LOAD VGGISH
	vggish_slim.define_vggish_slim(training=False)
	vggish_slim.load_vggish_slim_checkpoint(sess, params.VGG_CHECKPOINT_LOAD)
	features_tensor = sess.graph.get_tensor_by_name(vggish_params.INPUT_TENSOR_NAME)
	embedding_tensor = sess.graph.get_tensor_by_name(vggish_params.OUTPUT_TENSOR_NAME)

	# LOAD SKL MODEL
	logger.info("-- Setting Up SKL Model")
	train_stats = np.load(params.SKL_CHECKPOINT_STATS)
	skl_model = joblib.load(params.SKL_CHECKPOINT_MODEL)

	# LAUNCH API
	logger.info("-- Launching Sontirol API")
	app.run(debug=True,host="0.0.0.0")
