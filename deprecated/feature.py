import glob

from utils import augment
from utils import params
from utils import postprocess
from train import tf_vgg

# LOCATE RELAVENT DATA
fsave = "./output/pandas/embeddings_16bit_aug_filter.pickle"
wave_files = params.wavfiles
label_files = params.labfiles

#EXTRACT FEATURES
preds,labs = tf_vgg.feature_extract(wave_files,label_files,dsp=False,aug=True)
print "----- converting results to PD dataframe"
df = postprocess.panderize(preds,labs)
print "----- saving results to pickle"
df.to_pickle(fsave)