import sys
import glob
import time
import logging

import multiprocessing

def _get_mp3s():
	path = "../data/04-04-2018/"
	curr_files = set(glob.glob(path+"*.mp3"))
	print "Number of current Files: "+str(len(curr_files))
	return

if __name__ == "__main__":

	while True:
		p=None
		p = multiprocessing.Process(target=_get_mp3s)
		p.start()
		p.join(3)

		if p.is_alive():
			print "SonIP Process Timed Out - Restarting"
			p.terminate()
			p.join()
		else:
			print "Seems to have completed"