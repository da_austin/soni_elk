#!/usr/bin/python

"""
demo.py: Simple demo script to copy files from a starting directory
to a destination directory (the destination directory is 
being monitored by the sonitrol-listener.py file). It reads
all files in from the starting directory and copies them
iteratively to the destination directory according to a random 
interval from 0 to 2 seconds. 
"""

__author__ = "Jacob Tutmaher"
__copyright__ = "Copyright 2018, Stanley Black & Decker"
__date__="04/09/2018"

__version__ = "1.0"
__maintainer__ = "Stanley Black & Decker | Robert Coop"
__email__ = "robert.coop@sbdinc.com"
__status__ = "Prototype"

import time
import glob
from shutil import copyfile
import random 

# Define Directories
start_prefix = "/home/ubuntu/test/"
dest_prefix = "/home/ubuntu/demo/"

# Test file array were files held out during training. 
# The file list was saved to this numpy binary
test_files = sorted(glob.glob(start_prefix+"*"))

# Iterate through test files and copy them over
# to a destination folder
for tfile in test_files:

	# Generate random wait parameter
	wait = random.randint(0,2)
	print wait

	# Parse name of file
	name = tfile.split('/')[-1]
	dest = dest_prefix+name
	print tfile
	print dest

	# Copy file to monitored directory
	copyfile(tfile,dest)
	time.sleep(wait)
